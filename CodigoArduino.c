#include <avr/io.h>
#include <Arduino.h>
#include <math.h>
#include <Wire.h>
#include <OneWire.h> //Se importan las librerías
#include <DallasTemperature.h>
#define Pin 2 //Se declara el pin donde se conectará la DATA
OneWire ourWire(Pin); //Se establece el pin declarado como bus para la
comunicación OneWire
DallasTemperature sensors(&ourWire); //Se instancia la librería
DallasTemperature
static const int pinFPWM = 45;
double Duty = 0.9;
int PinVoltajeP = A0;
int PinCorrienteP = A1;
int PinCorrienteB = A2;
int PinVoltajeB = A3;
int PinSensorP = A4;
int PinSensorT = A5;
double dato=0;
double potenciaS=0;
double voltaje=0;
double voltajeB=0;
double voltajeS=0;
double voltajeT=0;
double corriente=0;
double corrienteB=0;
double potencia=0;
double temperaturaS=0;
double temperatura=0;
double sumavoltaje = 0;
double sumavoltajeB = 0;
double sumavoltajeS = 0;
double sumavoltajeT = 0;
double sumacorriente = 0;
double sumacorrienteB = 0;
double aux1=0;
131
//double deltaD = 0.01875; //Para 300KHz
//double deltaD = 0.00625; //Para 100KHz
//double deltaD = 0.003125; //Para 50KHz
double deltaD = 0.00125; //Para 20KHz
//double deltaD = 0.000625; //Para 10KHz
//double deltaD = 0.0003125; //Para 5KHz
int frec=800; // 16MHZ/300KHz=53---16MHz/100KHz=160---16MHz/50KHz=320---
16MHz/20KHz=800---16MHz/10KHz=1600---16MHz/5KHz=3200
int valor;
char comando;
char funcion;
int a=4;
//Pesos Red Neuronal Sensor
//Capa oculta
double capa_s1[10][2] = {
 {4.374356,-0.981079},
 {-0.539828,0.012861},
 {4.142616,-1.561642},
 {-2.834731,3.400632},
 {-25.333180,-0.799801},
 {4.352584,0.809326},
 {2.736363,-3.480275},
 {3.532404,2.668729},
 {-1.551299,-4.146500},
 {4.303044,-1.041059},
 };
double capa_s1_aux[10][2];
double bias_s1[10]={-4.440033,0.361952,-2.459549,1.475729,-
0.690687,0.491909,1.475729,2.459549,-3.443369,4.427188};
double salida_s1[10];
double salida_s1_aux[10];
//Capa de salida
double capa_s2[10]={-2.923258,-30.362868,-2.566049,3.246265,-
3.590747,2.702523,-3.452283,3.433685,-1.777298,-2.784702};
double bias_s2=2.336044;
//Pesos Red Neuronal Controlador
//Capas ocultas
double capa_c11[10][4] = {
 {0.000509,-0.011709,0.026348,-0.014694},
 {-0.009009,0.005780,-0.016181,0.005743},
 {-0.025586,0.022471,-0.028829,0.030862},
 {-0.047186,0.008061,-0.008602,0.050862},
 {-0.020976,0.004428,-0.019771,0.023807},
 {-0.059892,0.016274,-0.006373,0.060023},
 {0.012197,0.010096,-0.020275,-0.023527},
 {-0.007718,0.023190,-0.028629,0.007992},
 {-0.033344,-0.016733,-0.019232,0.024592},
 {-0.006622,0.007756,-0.023086,0.015672}
 };
double capa_c11_aux[10][4];
double capa_c12[10][4] = {
 {1.210969,-21.529728,1.576573,-12.505258},
 {16.810520,14.436898,9.215834,0.062555},
 {-9.678582,21.047821,-16.921916,14.855208},
 {-21.650384,-17.580655,18.207826,20.441348},
132
 {13.207574,28.900657,-5.356472,15.383183},
 {13.924703,-11.382828,-24.018967,21.576292},
 {-19.807679,-11.368980,-21.046538,-12.515754},
 {21.604660,-12.184038,19.208294,-5.000430},
 {6.882929,-19.168309,-20.165158,-21.909329},
 {-1.247352,18.551127,20.724837,-2.071765}
 };
double capa_c12_aux[10][4];
double bias_c1[10] = {27.276171,-34.640244,-8.383086,0.453254,-46.474834,-
0.476213,59.974112,-21.431344,47.280616,-33.301136};
double salida_c1[10];
double salida_c1_aux[10];
double salida_c11[10];
double salida_c12[10];
//Capa de salida
double capa_c2[10] = {-0.018236,0.012013,-0.029089,-
0.021866,0.006541,0.024828,-0.000541,0.009341,0.000446,0.024604};
double bias_c2 = 0.901503;
// variables para calculo de la red de control
double delaypotencia[4]={40,40,40,40};
double delayduty[4]={Duty,Duty,Duty,Duty};
#if defined(ARDUINO_ARCH_SAMD)
// for Zero, output on USB Serial console, remove line below if using
programming port to program the Zero!
#define Serial SerialUSB
#endif
void setup() {
Serial.begin(9600);
sensors.begin(); //Se inician los sensores
pinMode(3, OUTPUT);
digitalWrite(3, LOW);
 // Activar el pin como salida
 pinMode(pinFPWM, OUTPUT);
 // FPWM modo 14, no-invertido, escalado 1
 TCCR5A = _BV(COM5B1) | _BV(WGM51);
 TCCR5B = _BV(WGM53) | _BV(WGM52) | _BV(CS50);// | _BV(CS50);
 //Frecuencia
 ICR5=frec; //fPWM=300KHz (Period = 3.333us Standard). 4999 es para
50 Hz. 3200 5k. 800 20k. 320 50k. 160 100k.
 // Duty-cycle
 //OCR5B = 1; // Valor para el Compare Match
 // Iniciar
 TCNT5 = 0;
 OCR5B=Duty*frec;
establecer();
}
void loop() {
 if(funcion=='p'){ //Inicio condicion
 if(comando=='a'){
133
 digitalWrite(2,LOW);
 establecer();
 funcion=='otro';
 }
sensors.requestTemperatures(); //Prepara el sensor para la lectura
temperaturaS=sensors.getTempCByIndex(0); //Se lee la temperatura en
grados Celsius
delay(1000);
//llamar red neuronal, sensor y controlador
sensado();
 if(temperaturaS==-127){
 temperatura=voltajeT;
 }
 else
 {
 temperatura=temperaturaS;
 }
sensor_neural();
aux1=4;
retardos();
controlador_neural();
OCR5B=Duty*frec;
 Serial.println("d");
 Serial.println("2");
 Serial.println(Duty*1000);
 Serial.println("v");
 Serial.println("2");
 Serial.println(voltaje*1000);
 Serial.println("c");
 Serial.println("2");
 Serial.println(corriente*1000);
 Serial.println("v");
 Serial.println("1");
 Serial.println(voltajeB*1000);
 Serial.println("c");
 Serial.println("1");
 Serial.println(corrienteB*1000);
 Serial.println("v");
 Serial.println("3");
 Serial.println(voltajeS*1000);
 Serial.println("t");
 Serial.println("1");
 Serial.println(temperaturaS*1000);
 Serial.println("t");
 Serial.println("2");
 Serial.println(voltajeT*1000);
 Serial.println("p");
 Serial.println("1");
 Serial.println(potenciaS*1000);

 // delay(250);
134
 Serial.flush();//limpiamos el serial.

} //fin condicion
}
///FUNCIONES IMPLEMENTADAS
///////////////////////////////////////////////////////////////////////////
//////////
void sensado(){
 for(int i = 0; i<40; i++) {
 sumacorriente = sumacorriente + (0.073982 * analogRead(PinCorrienteP)
- 37.5); //37.5 cálculo, 37.878 tanteo
 sumavoltaje = sumavoltaje + analogRead(PinVoltajeP)*0.024414;
 sumacorrienteB = sumacorrienteB + (0.073982 *
analogRead(PinCorrienteB) - 37.5); //37.5 cálculo, 37.878 tanteo
 sumavoltajeB = sumavoltajeB + analogRead(PinVoltajeB)*0.024414;
 sumavoltajeS = sumavoltajeS + analogRead(PinSensorP)*0.024414;
 sumavoltajeT = sumavoltajeT + analogRead(PinSensorT)*0.0048828125;
 delay(1);
//Fórmula sensado corriente
//Amperios=(0.0048828125 * analogRead(PinCorrienteP)- vcc/2)/(0.066) : 30A
sensibilidad sensor
 }

corriente=sumacorriente/40;
voltaje=sumavoltaje/40;
corrienteB=sumacorrienteB/40;
voltajeB=sumavoltajeB/40;
voltajeS=sumavoltajeS/40;
//voltajeS=3.2;
voltajeT=sumavoltajeT*2.5;
sumacorriente=0;
sumacorrienteB=0;
sumavoltaje=0;
sumavoltajeB=0;
sumavoltajeS=0;
sumavoltajeT=0;
}
///////////////////////////////////////////////////////////////////////////
////////
void sensor_neural(){
for(int i = 0; i<10; i++) {
 for(int j = 0; j<2; j++) {
 if(j==0){
 capa_s1_aux[i][j]=(capa_s1[i][j])*(voltajeS);
 }
 if(j==1){
 capa_s1_aux[i][j]=(capa_s1[i][j])*(temperatura);
 }
 }
 salida_s1_aux[i]=(capa_s1_aux[i][0]+capa_s1_aux[i][1]+bias_s1[i]);

}
aux1=1;
135
sacar_tan();
potenciaS=0;
for(int i = 0; i<10; i++) {
 salida_s1[i]=(salida_s1[i])*(capa_s2[i]);
 potenciaS=potenciaS+salida_s1[i];
}
 potenciaS=potenciaS+bias_s2;// Salida final
}
///////////////////////////////////////////////////////////////////////////
void controlador_neural(){
for(int i = 0; i<10; i++) {
 for(int j = 0; j<4; j++) {
 if(j==0){
 capa_c11_aux[i][j]=(capa_c11[i][j])*(delaypotencia[0]);
 capa_c12_aux[i][j]=(capa_c12[i][j])*(delayduty[0]);
 }
 if(j==1){
 capa_c11_aux[i][j]=(capa_c11[i][j])*(delaypotencia[1]);
 capa_c12_aux[i][j]=(capa_c12[i][j])*(delayduty[1]);
 }
 if(j==2){
 capa_c11_aux[i][j]=(capa_c11[i][j])*(delaypotencia[2]);
 capa_c12_aux[i][j]=(capa_c12[i][j])*(delayduty[2]);
 }
 if(j==3){
 capa_c11_aux[i][j]=(capa_c11[i][j])*(delaypotencia[3]);
 capa_c12_aux[i][j]=(capa_c12[i][j])*(delayduty[3]);
 }
 }
 salida_c11[i]=capa_c11_aux[i][0]+capa_c11_aux[i][1]+capa_c11_aux[i][2
]+capa_c11_aux[i][3];
 salida_c12[i]=capa_c12_aux[i][0]+capa_c12_aux[i][1]+capa_c12_aux[i][2
]+capa_c12_aux[i][3];
 salida_c1_aux[i]=salida_c11[i]+salida_c12[i]+bias_c1[i];
}
aux1=2;
sacar_tan();
Duty=0;
 for(int i = 0; i<10; i++) {
 salida_c1[i]=(salida_c1[i])*(capa_c2[i]);
 Duty=Duty+salida_c1[i];
}
 Duty=Duty+bias_c2;// Salida final Controlador
}
//////////////////////////////////////////////////////////////////////////
void retardos(){
 if(aux1==4){
 for(int i = 3; i>0; i--) {
 delaypotencia[i]=delaypotencia[i-1];
 delayduty[i]=delayduty[i-1];
 }
 delaypotencia[0]=potenciaS;
 delayduty[0]=Duty;
 }
136
 aux1=0;
}
//////////////////////////////////////////////////////////////////////////
void sacar_tan(){
if(aux1==1){
for(int i = 0; i<10; i++) {

 if (salida_s1_aux[i]>22){
 dato=22;
 }
 if(salida_s1_aux[i]<-75){
 dato=-75;
 }
 if(salida_s1_aux[i]>-75 && salida_s1_aux[i]<22){
 dato=salida_s1_aux[i];
 }
 salida_s1[i] = (exp(dato)- exp(-1*(dato)))/(exp(dato)+ exp(-1*(dato)));
}
aux1=0;
}
if(aux1==2){
for(int i = 0; i<10; i++) {

 if (salida_c1_aux[i]>22){
 dato=22;
 }
 if(salida_c1_aux[i]<-75){
 dato=-75;
 }
 if(salida_c1_aux[i]>-75 && salida_s1_aux[i]<22){
 dato=salida_c1_aux[i];
 }
salida_c1[i] = (exp(salida_c1_aux[i])- exp(-
1*(salida_c1_aux[i])))/(exp(salida_c1_aux[i])+ exp(-1*(salida_c1_aux[i])));
}
aux1=0;
}
}
///////////////////////////////////////////////////////////////////////
void establecer() {
while (Serial.available() <= 0) {
 Serial.println(a);
 //Serial.println('4.32v1'); //52 46 51 50 118 49 --- 30257
 delay(50);
}
}
//////////////////////////////////////////////////////////////////////
void serialEvent(){
if (Serial.available()){
 delay(50); //esperemos a que se terminen de recibir
 comando=Serial.read(); //leemos el primer datos byte-
 if(comando=='o' || comando=='p'){
137
 funcion=comando;
 digitalWrite(2, HIGH);
 }
}
}
