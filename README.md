# Diseño e Implementación de un Controlador Neuronal con Arduino para Maximizar la Potencia Entregada por un Módulo Solar Fotovoltaico a una Carga


- **Autor:** Deimer Horacio Sevilla Hernández
- **Director:** Carlos Arturo Robles Alagrín
- **Codirector:** John Alexander Taborda Giraldo

<br>

## Presentación

<div align="justify">
Se presenta el proyecto de investigación como opción de grado para ser realizado en el
programa de ingeniería electrónica de la Universidad del Magdalena, el cual hace parte de la
propuesta de investigación presentada por el Grupo de investigación de la matemática aplicada
MAGMA-Ingeniería en el marco de la convocatoria para la presentación de proyectos de
investigación Fonciencias 2015-2017.
Se propone la implementación de un controlador neuronal artificial en Arduino, con el fin de
hacer un seguimiento del punto de máxima potencia de un módulo fotovoltaico y de esta forma
maximizar la potencia entregada a una carga para condiciones climáticas cambiantes con el
tiempo.
El controlador neuronal artificial se presenta como una alternativa al tradicional método de
Perturbación y Observación (P&O), el cual presenta problemas de estabilidad alrededor del punto
de operación cuando existen cambios súbitos en las condiciones ambientales a las que esté
expuesto un módulo fotovoltaico (FV). El controlador será implementado en las plataformas de
hardware libre Arduino y recibirá como señales de entrada la corriente y el voltaje del módulo
fotovoltaico. Los resultados obtenidos con el controlador para diferentes condiciones de operación,
serán enviados al ordenador por medio de módulos inalámbricos de transmisión-recepción.
Para cumplir con los objetivos de la presente propuesta, se plantean seis fases de trabajo que
inician con la etapa de investigación y selección de componentes, y finalizan con la evaluación del
desempeño del controlador neuronal implementado. De este modo, se plantea que esta
investigación es del tipo proyectiva en virtud de que surge como solución a un problema de tipo
práctico en un área particular del conocimiento.
En la fase de evaluación se medirá la eficiencia del controlador neuronal artificial realizando
pruebas entre dos sistemas FV con las mismas características, con la única diferencia que en un
sistema se tendrá el controlador Neuronal Artificial y en el otro un controlador con el algoritmo
de P&O. Para finalizar, se realizará la divulgación de los resultados ante la comunidad académica
del programa.
</div>

<br>

## Contenido
- **Documento informe final:** [Descargar aquí.](https://gitlab.com/magma-ingenieria/controlador-narx-para-mppt/-/raw/main/Controlador_NARX_para_MPPT.pdf?inline=false)
- **Anexo B:** Archivos de MATLAB y Simulink para la simulación de los modelos. [Consultar aquí.](https://gitlab.com/magma-ingenieria/controlador-narx-para-mppt/-/tree/main/Anexo%20B)
- **Código Arduino:** Código completo del controlador NARX desarrollado en Arduino. [Descargar aquí.](https://gitlab.com/magma-ingenieria/controlador-narx-para-mppt/-/raw/main/CodigoArduino.c?inline=false)
- **Diseño del circuito impreso:** Diseño de la tarjeta de circuito impreso y esquemático. Escudo_Energy_Solar_MEGA_RV3_1.brd y Escudo_Energy_Solar_MEGA_RV3_1.sch

## Tecnologías
- **MATLAB y Simulink:** R2013b. Nota: Se recomienda correr Matlab sobre un Sistema Operativo Windows XP.
- **Software para el diseño de circuitos:** Eagle.
