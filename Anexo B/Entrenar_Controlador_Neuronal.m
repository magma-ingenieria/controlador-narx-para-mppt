clear all;
close all;
clc;

%% Neuro control versi�n 2:
% Control inverso.
% El objetivo es dise�ar una RNA con din�mica inversa a la planta.
% 
%% Input:

[FileName Path]=uigetfile({'*.xlsx'},'Archivo Excel Entrenar controlador');
base=xlsread(FileName);
sim1=length(base);
% Duty2   = [base(1:sim1,1)'];
% Voltaje2   = [base(1:sim1,2)'];
% Corriente2 = [base(1:sim1,3)'];
% Potencia2 = [base(1:sim1,4)'];
% Voltaje1   = [base(1:sim1,5)'];
% Corriente1 = [base(1:sim1,6)'];
% Potencia1 = [base(1:sim1,7)'];
% Voltaje3   = [base(1:sim1,8)'];
% Temperatura1   = [base(1:sim1,9)'];
% Temperatura2   = [base(1:sim1,10)'];

Voltaje   = [base(1:sim1,1)'];
Corriente   = [base(1:sim1,2)'];
Potencia2 = [base(1:sim1,3)'];
Irradiacion = [base(1:sim1,4)'];
Temperatura   = [base(1:sim1,5)'];
Duty2 = [base(1:sim1,6)'];
Pmax = [base(1:sim1,7)'];

% plot(Duty2);
% figure;
% plot(Potencia2);

% voltaje=dat1(:,1)';
% corriente=dat1(:,2)';
% potencia=dat1(:,3)';
% cicloutil=dat1(:,4)';
%plot(Potencia2);
% p=[voltaje;corriente;potencia];
% t=cicloutil;
%% Input & Target

% for i=1:length(Voltaje2)
%     A = mat2cell([Potencia2(i)]);%([Voltaje2(i); Corriente2(i);Potencia2(i)]);% 
%     panelInput(i) = A;
% end

% [X,T] = simplenarx_dataset;
% load pollution_dataset.MAT
panelInput =num2cell(Potencia2);
panelTarget = num2cell(Duty2);

inputSeries = panelInput;
targetSeries = panelTarget;

% inputSeries = X;
% targetSeries = T;

%% Create a Nonlinear Autoregressive Network with External Input
inputDelays = 1:4;
feedbackDelays = 1:4;
hiddenLayerSize = 14;
net = narxnet(inputDelays,feedbackDelays,hiddenLayerSize);

% Choose Input and Feedback Pre/Post-Processing Functions
% Settings for feedback input are automatically applied to feedback output
% For a list of all processing functions type: help nnprocess
% Customize input parameters at: net.inputs{i}.processParam
% Customize output parameters at: net.outputs{i}.processParam
net.inputs{1}.processFcns = {'removeconstantrows','mapminmax'};
net.inputs{2}.processFcns = {'removeconstantrows','mapminmax'};

% Prepare the Data for Training and Simulation
% The function PREPARETS prepares timeseries data for a particular network,
% shifting time by the minimum amount to fill input states and layer states.
% Using PREPARETS allows you to keep your original time series data unchanged, while
% easily customizing it for networks with differing numbers of delays, with
% open loop or closed loop feedback modes.
[inputs,inputStates,layerStates,targets] = preparets(net,inputSeries,{},targetSeries);

%% Setup Division of Data for Training, Validation, Testing
% The function DIVIDERAND randomly assigns target values to training,
% validation and test sets during training.
% For a list of all data division functions type: help nndivide
net.divideFcn = 'dividerand';  % Divide data randomly
% The property DIVIDEMODE set to TIMESTEP means that targets are divided
% into training, validation and test sets according to timesteps.
% For a list of data division modes type: help nntype_data_division_mode
net.divideMode = 'value';  % Divide up every value
net.divideParam.trainRatio = 70/100;
net.divideParam.valRatio = 15/100;
net.divideParam.testRatio = 15/100;

%% Choose a Training Function
% For a list of all training functions type: help nntrain
% Customize training parameters at: net.trainParam
net.trainFcn = 'trainlm';  % Levenberg-Marquardt

%% Choose a Performance Function
% For a list of all performance functions type: help nnperformance
% Customize performance parameters at: net.performParam
net.performFcn = 'mse';  % Mean squared error

%% Choose Plot Functions
% For a list of all plot functions type: help nnplot
% Customize plot parameters at: net.plotParam
net.plotFcns = {'plotperform','plottrainstate','plotresponse', ...
  'ploterrcorr', 'plotinerrcorr'};

%% Train the Network
[net,tr] = train(net,inputs,targets,inputStates,layerStates);

%% Test the Network
outputs = net(inputs,inputStates,layerStates);
errors = gsubtract(targets,outputs);
performance = perform(net,targets,outputs);

%% Recalculate Training, Validation and Test Performance
trainTargets = gmultiply(targets,tr.trainMask);
valTargets = gmultiply(targets,tr.valMask);
testTargets = gmultiply(targets,tr.testMask);
trainPerformance = perform(net,trainTargets,outputs);
valPerformance = perform(net,valTargets,outputs);
testPerformance = perform(net,testTargets,outputs);

%% View the Network
view(net)

%% Plots
% Uncomment these lines to enable various plots.
% figure, plotperform(tr)
% figure, plottrainstate(tr)
% figure, plotregression(targets,outputs)
% figure, plotresponse(targets,outputs)
% figure, ploterrcorr(errors)
% figure, plotinerrcorr(inputs,errors)

%% Simulation
output= sim(net,inputs,inputStates);
output = cell2mat(output);

hold on;
% plot(potencia,output)
% plot(potencia,cicloutil, 'r')
plot(output)
plot(Duty2, 'r')
%% Closed Loop Network
% Use this network to do multi-step prediction.
% The function CLOSELOOP replaces the feedback input with a direct
% connection from the outout layer.
netc = closeloop(net);
netc.name = [net.name ' - Closed Loop'];
view(netc)
[xc,xic,aic,tc] = preparets(netc,inputSeries,{},targetSeries);
yc = netc(xc,xic,aic);
closedLoopPerformance = perform(netc,tc,yc);

% Early Prediction Network
% For some applications it helps to get the prediction a timestep early.
% The original network returns predicted y(t+1) at the same time it is given y(t+1).
% For some applications such as decision making, it would help to have predicted
% y(t+1) once y(t) is available, but before the actual y(t+1) occurs.
% The network can be made to return its output a timestep early by removing one delay
% so that its minimal tap delay is now 0 instead of 1.  The new network returns the
% same outputs as the original network, but outputs are shifted left one timestep.
nets = removedelay(net);
nets.name = [net.name ' - Predict One Step Ahead'];
view(nets)
[xs,xis,ais,ts] = preparets(nets,inputSeries,{},targetSeries);
ys = nets(xs,xis,ais);
earlyPredictPerformance = perform(nets,ts,ys);
%%
gensim(netc);