s=1; % n�mero paneles en serie
p=1; % n�mero paneles en paralelo
Ei = 100;
Ein= 1000; % constante de irradiaci�n
Tn= 25; % constante de temperatura
%CV= 0.8;
b= 0.0684; % par�metro de ajuste
Isc= 8.40; % corriente de cortocircuito
Voc= 21.40; % voltaje de circuito abierto
V=18.00;%Voltaje panel
Vpmax= 18.00; % voltaje en el punto de m�xima potencia
Ipmax= 7.50; % corriente en el punto de m�xima potencia
T = 25;
TCv= -0.1261; % coeficiente de voltaje
TCi= 0.00418; % coeficiente de corriente
Vmax= Voc*1.03;
Vmin= Voc*0.5;


Ix= p*(Ei/Ein)*(Isc+(TCi*(T-Tn)));
Vx= (s*(Ei/Ein)*(TCv)*(T-Tn))+(s*Vmax)-((s*(Vmax-Vmin))*(exp((Ei/Ein)*(log((Vmax-Voc)/(Vmax-Vmin))))));
step=0.01;
Vi=0:step:Vx;
Iv= ((Ix/(1-(exp(-1/b))))*(1-(exp((Vi/(b*Vx))-(1/b)))));
P = Vi.*Iv;
figure;
plot(P)
